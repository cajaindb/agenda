
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>registrar</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
</style>
</head>
    <body>
        <div class="login-form">

            <form action="/register/user" method="POST">

                <h2 class="text-center">Registrar-se</h2>     

                {{ csrf_field() }}


                @if (count($errors) > 0 )
                <div class="alert alert-danger">

                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>    
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="form-group">

                    <input name="user_name" type="text" class="form-control" placeholder="Nome de usuário" required="required">
    
                </div>

                <div class="form-group">

                    <input name="email" type="email" class="form-control" placeholder="endereço de email" required="required">

                </div>

                {{-- <div class="form-group">

                    <input name="email" type="email" class="form-control" placeholder="confirme seu endereço de email" required="required">
    
                </div> --}}

                <div class="form-group">

                    <input name="password" type="password" class="form-control" placeholder="senha" required="required">
        
                </div> 

                <div class="form-group">

                    <input name="passwordconfirm" type="password" class="form-control" placeholder="confirme sua senha" required="required">
             
                </div>



                <div class="form-group">

                    <button type="submit" class="btn btn-primary btn-block">sign in</button>

                </div>   

            </form>
            
        </div>
    </body>
</html>   
