<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\AuthenticatesUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\registrationvalidation;

class AuthController extends Controller
{
    protected $auth;

    public function __construct(AuthenticatesUser $auth)
    {
        $this->auth = $auth;
    }

    public function dashboard()
    {
        return view('dashboard');
    }






    public function register()
    {
        return view('register');
    }

    public function store(registrationvalidation $request){

        $data = $request->all();

        User::create([
            'user_name' => $data['user_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);        

        return redirect()->back();
    }

    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
