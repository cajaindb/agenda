<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\registrationvalidation;
use App\Http\Requests\loginvalidator;

class LoginController extends Controller
{
    
    public function login()
    {
        return view('login');
    } 

       /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(loginvalidator $request)
    {
        
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
        else{
            session()->flash('error', 'email ou senha incorreta');
            return back();
        }
    }
   
}
