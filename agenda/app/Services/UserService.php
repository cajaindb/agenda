<?php

namespace App\Services;

use App\Repositories\UserRepositoryEloquent;

class UserService
{
    /** @var UserRepositoryEloquent */
    protected $userRepositoryEloquent;

    public function __construct(UserRepositoryEloquent $userRepositoryEloquent)
    {
        $this->userRepositoryEloquent = $userRepositoryEloquent;
    }
}